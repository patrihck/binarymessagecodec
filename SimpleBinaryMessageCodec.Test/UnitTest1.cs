using System.Text;
using SimpleBinaryMessageCodec.Model;

namespace SimpleBinaryMessageCodec.Test;

public class Tests
{
    [SetUp]
    public void Setup()
    {
    }

    [Test]
    public void Test1()
    {
        var codec = new Logic.SimpleBinaryMessageCodec();
        var message = new Message
        {
            Headers = new Dictionary<string, string>
            {
                { "Header1", "Value1" },
                { "Header2", "Value2" },
            },
            Payload = Encoding.ASCII.GetBytes("Example payload")
        };

        byte[] encodedMessage = codec.Encode(message);
        Message decodedMessage = codec.Decode(encodedMessage);

    }
}