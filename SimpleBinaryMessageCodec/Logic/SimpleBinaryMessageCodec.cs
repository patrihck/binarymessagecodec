﻿using System.Text;
using SimpleBinaryMessageCodec.Interface;
using SimpleBinaryMessageCodec.Model;

namespace SimpleBinaryMessageCodec.Logic;

public class SimpleBinaryMessageCodec : IMessageCodec
{
    public byte[] Encode(Message message)
    {
        using var ms = new MemoryStream();
        using var bw = new BinaryWriter(ms, Encoding.ASCII);

        bw.Write((byte)message.Headers.Count);

        foreach (var header in message.Headers)
        {
            WriteString(bw, header.Key);
            WriteString(bw, header.Value);
        }

        bw.Write((uint)message.Payload.Length);

        bw.Write(message.Payload);

        return ms.ToArray();
    }

    public Message Decode(byte[] data)
    {
        using var ms = new MemoryStream(data);
        using var br = new BinaryReader(ms, Encoding.ASCII);

        var message = new Message();

        int headerCount = br.ReadByte();

        for (int i = 0; i < headerCount; i++)
        {
            string headerName = ReadString(br);
            string headerValue = ReadString(br);

            message.Headers.Add(headerName, headerValue);
        }

        int payloadLength = (int)br.ReadUInt32();

        message.Payload = br.ReadBytes(payloadLength);

        return message;
    }

    private void WriteString(BinaryWriter writer, string value)
    {
        ushort length = (ushort)value.Length;
        writer.Write(length);
        writer.Write(Encoding.ASCII.GetBytes(value));
    }

    private string ReadString(BinaryReader reader)
    {
        ushort length = reader.ReadUInt16();
        return Encoding.ASCII.GetString(reader.ReadBytes(length));
    }
}