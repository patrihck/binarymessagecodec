﻿using SimpleBinaryMessageCodec.Model;

namespace SimpleBinaryMessageCodec.Interface;

public interface IMessageCodec
{
    byte[] Encode(Message message);
    Message Decode(byte[] data);
}